//
//  HWHomeworkView.m
//  Homework1
//
//  Created by Vladislav Grigoriev on 20/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import "HWHomeworkView.h"

@interface HWHomeworkView ()

@property (nonatomic, strong) NSMutableArray<UIView *> *mutableViews;
@property (nonatomic, strong) UITapGestureRecognizer *endEditingTapGestureRecognizer;

@end

@implementation HWHomeworkView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor lightGrayColor];
        
        _mutableViews = [[NSMutableArray alloc] init];
        
        _contentInsets = UIEdgeInsetsMake(HWDefaultMargin + [UIApplication sharedApplication].statusBarFrame.size.height,
                                          HWDefaultMargin,
                                          HWDefaultMargin,
                                          HWDefaultMargin);
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
        _scrollView.alwaysBounceVertical = YES;
        [_scrollView hw_setShadow];
        [self addSubview:_scrollView];
        
        _endEditingTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shouldEndEditing)];
        [self addGestureRecognizer:_endEditingTapGestureRecognizer];
    }
    return self;
}

#pragma mark - Public methods

- (void)addView:(UIView *)view {
    [self.scrollView addSubview:view];
    view.layer.cornerRadius = 5.0f;
    [_mutableViews addObject:view];
    [self setNeedsLayout];
}

#pragma mark - Accessors

- (void)setViews:(NSArray<UIView *> *)views {
    [_mutableViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [_mutableViews removeAllObjects];
    
    [views enumerateObjectsUsingBlock:^(UIView * _Nonnull view, NSUInteger idx, BOOL * _Nonnull stop) {
        [self.scrollView addSubview:view];
        view.layer.cornerRadius = 5.0f;
        [_mutableViews addObject:view];
    }];
    [self setNeedsLayout];
}

- (NSArray<UIView *> *)views {
    return [self.mutableViews copy];
}

#pragma mark - Overrite

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect actualBounds = UIEdgeInsetsInsetRect(self.bounds, self.contentInsets);
    
    CGFloat offset = HWDefaultMargin;
    __block CGPoint viewPosition = CGPointMake(actualBounds.origin.x, actualBounds.origin.y);
    
    [self.views enumerateObjectsUsingBlock:^(UIView * _Nonnull view, NSUInteger idx, BOOL * _Nonnull stop) {
        CGSize viewSize = [view sizeThatFits:CGSizeMake(actualBounds.size.width, CGFLOAT_MAX)];
        view.frame = CGRectMake(viewPosition.x,
                                viewPosition.y,
                                actualBounds.size.width,
                                viewSize.height);
        viewPosition.y += viewSize.height + offset;
    }];
    
    self.scrollView.frame = self.bounds;
    self.scrollView.contentSize = CGSizeMake(self.bounds.size.width, viewPosition.y - offset);
}

#pragma mark - Private methods

- (void)shouldEndEditing {
    if (self.delegate) {
        [self.delegate homeworkViewShouldEndEditing:self];
    }
}

@end
